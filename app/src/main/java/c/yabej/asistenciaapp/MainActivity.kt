package c.yabej.asistenciaapp

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import io.github.rybalkinsd.kohttp.ext.httpGet
import kotlinx.coroutines.GlobalScope
import okhttp3.Response
import org.jetbrains.anko.toast
import kotlinx.coroutines.launch
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class MainActivity : AppCompatActivity() {

     var mBluetoothAdapter: BluetoothAdapter? = null
     val REQUEST_ENABLE_BLUETOOTH = 0
     val REQUEST_ENABLE_DISCOVERY = 1
     var asistencia = 0


    @RequiresApi(Build.VERSION_CODES.O)
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val myBtn = findViewById<Button>(R.id.buttonBlue)
        val asistBtn = findViewById<Button>(R.id.Asistencia)
        val btnOff = findViewById<Button>(R.id.buttonOff)

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(mBluetoothAdapter == null){
            toast("Este dispositivo no sporta bluetooth")
            return
        }
        if(!mBluetoothAdapter!!.isEnabled){
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BLUETOOTH)
        }


        myBtn.setOnClickListener{ DiscoverDevices() }
        btnOff.setOnClickListener{ OffBluetooth() }
        asistBtn.setOnClickListener{ Asistencia() }
    }

    private fun Asistencia(){
        val intent = Intent(this, Absent::class.java)
        startActivity(intent)
    }


    private fun OffBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        mBluetoothAdapter!!.disable()
        toast("Bluetooth is disabled")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @ExperimentalStdlibApi
    private fun DiscoverDevices() {
        if (mBluetoothAdapter!!.isDiscovering) {
            mBluetoothAdapter!!.cancelDiscovery()
        }

        val getVisible = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible,REQUEST_ENABLE_DISCOVERY)
        Thread.sleep(5000)
         sendGet()


    }

    @RequiresApi(Build.VERSION_CODES.O)
    @ExperimentalStdlibApi
    fun sendGet() {

        GlobalScope.launch{

            val ma = findViewById<ConstraintLayout>(R.id.mainAc)

            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("d-M-yyyy")
            val formatted = current.format(formatter)
            var fechaF = formatted.split('-').toMutableList()
            var dia: Int = fechaF[1].toInt() -1
            fechaF[1] = dia.toString()
            val newDate = fechaF[0] + "-"+fechaF[1]+"-"+fechaF[2]
            try
            {
                val asist = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$newDate/students/201511498\n").readBytes()
                when{
                    asist.decodeToString().contains("Yesid") ->
                        ma.setBackgroundColor(Color.GREEN)
                }
            }catch (e: Exception){
                ma.setBackgroundColor(Color.RED)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == REQUEST_ENABLE_BLUETOOTH){
            if(resultCode == Activity.RESULT_OK){
                if(mBluetoothAdapter!!.isEnabled){
                    toast("Bluetooth enabled")
                } else{
                    toast("Bluetooth disabled")
                }
            } else if (resultCode == Activity.RESULT_CANCELED){
                toast("Bluetooth canceled")
            }
        }
    }
}
