package c.yabej.asistenciaapp

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.*
import androidx.core.view.get
import androidx.core.view.size
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.Parser
import io.github.rybalkinsd.kohttp.util.Json
import kotlinx.android.synthetic.main.activity_absent.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.toast
import org.json.JSONArray
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.lang.StringBuilder
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import com.beust.klaxon.Parser.Companion as Parser1

@Suppress("UNCHECKED_CAST")
class Absent : AppCompatActivity(), View.OnClickListener {


    val c = Calendar.getInstance()
    val mes = c.get(Calendar.MONTH)
    val dia = c.get(Calendar.DAY_OF_MONTH)
    val anio = c.get(Calendar.YEAR)
    var edFecha : EditText? = null
    var datee : String = ""

    var proAdap: ArrayAdapter<String>? = null
    private lateinit var lvAbsent :ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absent)

        edFecha = findViewById<EditText>(R.id.et_mostrar_fecha_picker)
        lvAbsent = findViewById<ListView>(R.id.listA)
        val ibObtenerFecha = findViewById<ImageButton>(R.id.ib_obtener_fecha)
        val btnExport = findViewById<Button>(R.id.export)


        ibObtenerFecha.setOnClickListener(this)
        btnExport.setOnClickListener{ exportToCSV() }
    }

    @ExperimentalStdlibApi
    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.ib_obtener_fecha -> obtenerfecha()
        }

    }

    private fun exportToCSV(){
        var fileWriter: FileWriter? = null
        val path = this.getExternalFilesDir(String())

        var file = File("$path\\absent.csv")
        file.createNewFile()
        try{
            fileWriter = FileWriter("$path\\absent.csv")
            fileWriter.append("Name,Code")
            fileWriter.append('\n')
            for(item in 0 until  lvAbsent.size){
                val itemact: String = listA.getItemAtPosition(item) as String
                val split = itemact.split("-")
                fileWriter.append(split[0])
                fileWriter.append(",")
                fileWriter.append(split[1])
                fileWriter.append('\n')
            }
            toast("File created succesfully")
        }catch (e: Exception){
            e.printStackTrace()
        } finally {
            try{
                fileWriter!!.flush()
                fileWriter.close()
            } catch (e:IOException){
                e.printStackTrace()
            }
        }
    }

    @ExperimentalStdlibApi
    private fun obtenerfecha(){
        val recogerFecha =  DatePickerDialog(this, DatePickerDialog.OnDateSetListener{ view,mYear,mMonth,mDay ->
              edFecha!!.setText(""+mDay+"-"+mMonth+"-"+ mYear)

               datee = edFecha!!.text.toString()
               callApi(datee)
            }, anio, mes, dia)

        recogerFecha.show()


    }

    @ExperimentalStdlibApi
    private fun callApi(datee: String?) {
        GlobalScope.launch {
            val noAsistieron: MutableList<String> = ArrayList<String>()
            val totalStudents = URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList?pageSize=1000").readText()
            val asist =
                URL("https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/$datee/students/\n").readText()

            val parser1: Parser = Parser()
            val stringBuilder1: StringBuilder= StringBuilder(totalStudents)
            val json1:JsonObject = parser1.parse(stringBuilder1) as JsonObject
            val jsonArrayStudents = json1.get("documents") as JsonArray<*>

            val parser: Parser = Parser()
            val stringBuilder: StringBuilder= StringBuilder(asist)
            val json:JsonObject = parser.parse(stringBuilder) as JsonObject
            val jsonArrayAsist = json.get("documents") as JsonArray<*>

            for(i in 0 until jsonArrayStudents.size){
                val act: JsonObject= jsonArrayStudents[i] as JsonObject
                val actfield = act.get("fields") as JsonObject

                val actname = actfield.get("name") as JsonObject
                val actname2 =actname.get("stringValue")

                val actcode = actfield.get("code") as JsonObject
                val actcode2 = actcode.get("stringValue")

                val resp: String = "$actname2  -   $actcode2"
                noAsistieron.add(resp)

            }
            for(i in 0 until jsonArrayAsist.size){
                val item = jsonArrayAsist[i] as JsonObject
                val actfield = item.get("fields") as JsonObject
                val actcode = actfield.get("code") as JsonObject
                val actcode2 =  actcode.get("stringValue") as String
                val thingsToRemove: MutableList<String> = ArrayList<String>()
                val iterate = noAsistieron.listIterator()
                while(iterate.hasNext()){
                    val old = iterate.next()
                    if(old.contains(actcode2)){
                        thingsToRemove.add(old)
                    }
                }
                noAsistieron.removeAll(thingsToRemove)

            }
            when {
               noAsistieron!=null ->
                    proAdap = ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, noAsistieron)

                else -> println("FALSE")
            }
        }
        Thread.sleep(5000)

        lvAbsent.adapter = proAdap
    }


}
